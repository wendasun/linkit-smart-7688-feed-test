#!/bin/sh
GPIO=$1
VALUE=$2
if [ -n "$(echo $GPIO| sed -n "/^[0-9]\+$/p")" ];then
  echo "$GPIO is number."
else
  echo 'no.'
  exit
fi
if [ "a$VALUE" != "a1" ] && [ "a$VALUE" != "a0" ]
then
exit
fi
if [ ! -d /sys/class/gpio/gpio$GPIO ]
then
echo $GPIO > /sys/class/gpio/export
echo out > /sys/class/gpio/gpio$GPIO/direction
echo $VALUE > /sys/class/gpio/gpio$GPIO/value
else
echo $VALUE > /sys/class/gpio/gpio$GPIO/value
fi

