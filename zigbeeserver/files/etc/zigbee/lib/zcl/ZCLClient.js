'use strict';
var util = require('util');
var debug = require('debug')('ZCLClient');
var Dissolve = require('dissolve');
var Concentrate = require('concentrate');
var when = require('when');

var ZNPClient = require('../znp/ZNPClient');
var ZCL = require('./constants');
var packets = require('./packets');
var DataTypes = require('./DataTypes');
var profileStore = require('../profile/ProfileStore');



/**
 * Extends ZNPClient to provice the ZCL layer, without understanding any of the underlying hardware.
 */
function ZCLClient(config) {
  ZNPClient.call(this, config);

  // handle zcl messages
  this.on('incoming-message', this._handleIncomingMessage.bind(this));

  // handle attribute reports
  this.on('zcl-command:ReportAttributes', this._handleReportAttributes.bind(this));

  // XXX: This really should be pulled out and handled by the user, or at least put into a IASZone mixin
  this.on('zcl-command:IAS Zone.Zone Enroll Request', this._handleZoneEnrollRequest.bind(this));

  //设备状态改编后消息上报 处理
  this.on('zcl-command:IAS Zone.Zone Status Change Notification', this._handleZoneStatusChangeNotification.bind(this));

  //小米设备加入
  this.on('zcl-command:MIADD', this._handleMiAdd.bind(this));

  //小米设备事件上报
  this.on('zcl-command:Mi Send Event', this._handleMiEvent.bind(this));

  //无线充电设备事件上报
  this.on('zcl-command:Wireless Charging Event', this._handleWirelessChargingEvent.bind(this));

  //xxx:Af Response
  if(this.config.AFDataRequest)
  {
    this.on('AFDate-response',this.config.AFDataRequest.bind(this));
  }
  
}

util.inherits(ZCLClient, ZNPClient);

ZCLClient.prototype._inFlight = new Array(256);
ZCLClient.prototype._sequence = 1;

var ZCL_RESPONSE_TIMEOUT = 20 * 1000;

ZCLClient.prototype.nextSequenceNumber = function() {
  if (++this._sequence > 255) {
    this._sequence = 1;
  }
  return this._sequence;
};




ZCLClient.prototype.sendZCLFrame = function(params, afParams,CallBack) {

  CallBack = CallBack || "NoCallBack";

  debug('sendZCLFrame', 'ZCL data >>', params);

  var sequenceNumber = this.nextSequenceNumber();
  if (this._inFlight[sequenceNumber]) {
    console.log(
      'WARN: Too many pending requests! Sequence number ' + sequenceNumber + 'has not had a response ' +
      'and has not timed out. Consider lowering the timeout.'
    );
  }

  params.TransactionSequenceNumber = sequenceNumber;

  var zclFrame = packets.ZCL_FRAME.write(params);

  var deferred = when.defer();

  var clearSequenceNumber = function() {
    delete this._inFlight[sequenceNumber];
  }.bind(this);

  var fail = function(reason) {

    clearSequenceNumber();
    deferred.reject(reason);

  }.bind(this);

  this._inFlight[sequenceNumber] = deferred;

  afParams.payload = zclFrame;
  this.sendAFDataRequest(afParams)
    .then(function(status) {


        if(CallBack !== "NoCallBack") {
          CallBack(status);
        }else{
          console.log("Not Set SendAFCMD CallBack Function");
        }

         if (status.key !== 'ZSuccess') {
            throw new Error(status);
       }
    })
    .done(function() {}, fail);

  return deferred.promise.timeout(ZCL_RESPONSE_TIMEOUT)
    .tap(clearSequenceNumber)
    .catch(clearSequenceNumber);
};





ZCLClient.prototype._handleIncomingMessage = function(message) {

  var self = this;

  debug('_handleMessage', message);

  // Parse the ZCL header...
  // TODO: Move this out?
  var zclParser = Dissolve()
    .uint8('frameControl')
    //XXX: .uint16le('manufacturerCode') How do I know if this is here??
    .uint8('sequenceNumber')
    .uint8('commandIdentifier')
    .buffer('payload', message.data.length - 3)
    .tap(function() {
      var zclMessage = this.vars;

      debug('_handleMessage', 'clusterId: 0x', message.clusterId.toString(16), 'command: 0x' + zclMessage.commandIdentifier.toString(16));

        zclMessage.commandName;
        zclMessage.headers = message;
      // XXX: FIX THIS! The commands should be listed in zcl.xml, server (incoming) and client (outgoing) separated.
      if (message.clusterId === 0) {
            // It's a general command... just grab from the enums for now
      try {
        zclMessage.commandName = zclMessage.command = ZCL.GeneralCommands.get(zclMessage.commandIdentifier).key;
      }catch(err){
          console.log(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
          console.log(zclMessage);
          console.log("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
          console.log('ZCLClient >>>> 151 line',err);
        }
      } else if (message.clusterId === 0x500 && zclMessage.commandIdentifier === 0) {
        zclMessage.commandName = 'IAS Zone.Zone Status Change Notification';
        self.emit('zcl-command:IAS Zone.Zone Status Change Notification', zclMessage);
      } else if (message.clusterId === 0x500 && zclMessage.commandIdentifier === 1) {
        zclMessage.commandName = 'IAS Zone.Zone Enroll Request';
        self.emit('zcl-command:IAS Zone.Zone Enroll Request', zclMessage);//处理Ias Zone.Zone Enroll Request
      } else if(message.clusterId === 0x06 && zclMessage.commandIdentifier === 10 ){
        //小米设备上报Event 事件处理
        zclMessage.commandName = 'Mi send Event';
        self.emit('zcl-command:Mi Send Event', zclMessage);
      }else if(message.clusterId === 0x08 && zclMessage.commandIdentifier === 10){
        //无线充电事件上报
        zclMessage.commandName = 'Wireless Charging Event';
        self.emit('zcl-command:Wireless Charging Event', zclMessage);
      }else {
        //console.log('XXX: Unknown incoming ZCL command. zcl.xml needs to be fixed! Do it!');
        zclMessage.commandName = 'UNKNOWN COMMAND';
      }


      if(zclMessage.commandName == 'MIADD'){
        //小米事件上报监听
        self.emit('zcl-command:MIADD', zclMessage);
      }


      //var cluster = profileStore.getCluster(message.clusterId);
      //debug('_handleMessage', 'cluster:', cluster);
      //zclMessage.command = ZCL.GeneralCommands.get(zclMessage.commandIdentifier);

      debug('_handleMessage', 'parse ZCL', zclMessage);

      debug('_handleMessage', 'Emitting to ', 'command.' + zclMessage.headers.srcAddr + '.' + zclMessage.headers.srcEndpoint + '.' + zclMessage.headers.clusterId);
      // XXX: Send it up to the cluster so the user can see it.
      self.emit('command.' + zclMessage.headers.srcAddr + '.' + zclMessage.headers.srcEndpoint + '.' + zclMessage.headers.clusterId, zclMessage);

      if (zclMessage.sequenceNumber) {
        // It's a reply
        var handler = self._inFlight[zclMessage.sequenceNumber];

        if (!handler) {
		if(self.config.NodeMsgHandle){
          zclMessage.commandName = 'Node Report Message';
          //self.config.NodeMsgHandle(zclMessage);
          return 0;
		}else{
		  return console.error('No handler for ZCL message', zclMessage);
			}
        }

        handler.resolve(zclMessage);
      } else {
        // Pass it along
        if (!self.emit('zcl-command:' + zclMessage.commandName, zclMessage)) {


          if(self.config.NodeMsgHandle){
            zclMessage.commandName = 'Node Report Message';
           // self.config.NodeMsgHandle(zclMessage);
            return 0;
          }else{
            console.log('Zcl command had no listeners!', zclMessage);
          }

        }
      }

    })
    .write(message.data);

};


ZCLClient.prototype._handleZoneStatusChangeNotification = function(message) {
  message.model='Yongmin';
  this.config.NodeMsgHandle(message);
}

ZCLClient.prototype._handleMiAdd = function(message) {
  if(this.config.NewFindDevices[message.headers.srcAddr.toString(16).toUpperCase()]) {
  this.config.NewFindDevices[message.headers.srcAddr.toString(16).toUpperCase()].typeZone=0x01;
  this.config.sendAddDevice(this.config.NewFindDevices[message.headers.srcAddr.toString(16).toUpperCase()]);
  }
}

ZCLClient.prototype._handleWirelessChargingEvent = function(message) {
  message.model='MK';
  this.config.NodeMsgHandle(message);
}

ZCLClient.prototype._handleMiEvent = function(message) {
  message.model='Mi';
  this.config.NodeMsgHandle(message);
}

// XXX: TODO: How do we use this properly?
var IAS_ZONE_ID = 123;

ZCLClient.prototype._handleZoneEnrollRequest = function(message) {

  var self = this;

  Dissolve()
    .uint16le('Zone Type')
    .uint16le('Manufacturer Code')
    .tap(function() {
      debug('_handleZoneEnrolRequest', 'IAS Zone enroll request from', message.headers.srcAddr, ' Manufacturer code:', this.vars['Manufacturer Code']);
        if(self.config.NewFindDevices[message.headers.srcAddr.toString(16).toUpperCase()]) {
          self.config.NewFindDevices[message.headers.srcAddr.toString(16).toUpperCase()].typeZone = this.vars['Zone Type'];
          self.config.sendAddDevice(self.config.NewFindDevices[message.headers.srcAddr.toString(16).toUpperCase()]);
        }
      // TODO: Read the zone type? We probably just want to allow all types anyway.

      // 8.2.2.3.1 Zone Enroll Response Command
      var payload = Concentrate()
        .uint8(0) // Enroll response code - 0 = success
        .uint8(1)//.uint8(IAS_ZONE_ID)
        .result();

      return self.sendZCLFrame({
        FrameControl: {
          ClusterSpecific: true
        },
        DeviceShortAddress: message.headers.srcAddr,
        CommandIdentifier: 0x00,
        payload: payload
      }, {
        DstAddr: {
          address: message.headers.srcAddr
        },
        DstEndpoint: message.headers.srcEndpoint,
        ClusterID: message.headers.clusterId,
        Options: {
          ackRequest: true,
          discoverRoute: true
        }
      });

    })
    .write(message.payload);
};

ZCLClient.prototype._handleReportAttributes = function(message) {

  Dissolve()
    .uint16le('attributeIdentifier')
    .uint8('attributeDataType')
    .tap(function() {
      debug('_handleReportAttributes', 'parsed ReportAttributes', this.vars);
    })
    .write(message.payload);
};


ZCLClient.prototype.Simple_Descriptor = function(shortAddr,endpoint) {
  debug(this, 'simpleDescriptor');

  var descReq = Concentrate()
      .uint16le(parseInt(shortAddr,16)) // DstAddr
      .uint16le(parseInt(shortAddr,16)) // NWKAddrOfInterest
      .uint8(endpoint) // Endpoint
      .result();
  return this.comms
      .request('ZDO_SIMPLE_DESC_REQ', descReq)
      .then(function(response) {
        if (response.data[0] !== 0x00) {
          throw new Error('Failed requesting Simple Descriptor');
        }
        //var deferred = when.defer();
        //
        //this.once('simpleDescriptor', deferred.resolve);
        //
        //return deferred.promise;
      }.bind(this));
};




ZCLClient.prototype.Find_ActiveEndpoints = function(shortAddr) {
  console.log('FindActiveEndpints :',shortAddr);
  var payload = Concentrate()
      .uint16le(parseInt(shortAddr,16)) // DstAddr
      .uint16le(parseInt(shortAddr,16)) // NWKAddrOfInterest
      .result();

  return this.comms
      .request('ZDO_ACTIVE_EP_REQ', payload)
      .then(this._parseStatus)
      .then(function(status) {
        debug(this, 'ZDO_ACTIVE_EP_REQ', 'status', status);
        if (status.key !== 'ZSuccess') {
          throw new Error('ZDO_ACTIVE_EP_REQ failed with error: ' + status.key);
        }

        return status;
      }.bind(this));
};




ZCLClient.prototype.FindMatchEndpoints = function(shortAddress,profileId, inClusters, outClusters) {
  console.log('FindMatchEndpints :',shortAddress);
  inClusters = inClusters || [];
  outClusters = outClusters || [];

  debug(this, 'findEndpoints', profileId.toString(16));

  var matchEndpointsPayload = Concentrate()
      .uint16le(parseInt(shortAddress,16)) // DstAddr
      .uint16le(parseInt(shortAddress,16)) // NWKAddrOfInterest
      .uint16le(profileId); // ProfileID

  matchEndpointsPayload.uint8(inClusters.length); // NumInClusters
  inClusters.forEach(function(c) {
    matchEndpointsPayload.uint16le(c);
  });

  matchEndpointsPayload.uint8(outClusters.length); // NumOutClusters
  outClusters.forEach(function(c) {
    matchEndpointsPayload.uint16le(c);
  });

  this.comms.request('ZDO_MATCH_DESC_REQ', matchEndpointsPayload.result());
};





module.exports = ZCLClient;
