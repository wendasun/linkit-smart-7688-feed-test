'use strict';

var ZCLClient = require('../lib/zcl/ZCLClient');
var glob = require('glob');
var _ = require('underscore');

var cfg ={};
cfg.panId=0xFFFF;
//devil 主动上报
cfg.NodeMsgHandle = function (msg){
        

        if(NodeMsgH_callback != null)
        {
          NodeMsgH_callback(msg);
        }else{
          console.log(msg);
        }
}
//发送onoff是否成功
cfg.AFDataRequest = function (msg){
    console.log("status" + msg.data[0] + " ss: " + msg.data.length);
    if(enableDevice_Status == 1){
      var msgs = msg.data == 0 ? 0 : 1;
      enableDevice_Callback(msgs);
      enableDevice_Status = 0;
      enableDevice_Callback = ''; 
    }
}


var client = new ZCLClient(cfg);
var deviceList = new Array();
var devices_describe  ={};
    devices_describe.DeviceList= deviceList;

var enableDevice_Status = '';
var enableDevice_Callback ='';
var NodeMsgH_callback = null;


console.log('CC2530/1 test app started!');

glob('{/dev/ttyS1,/dev/ttyS1}', function (err, devices) {
  
  client.connectToPort(devices[0])
    .then(client.firmwareVersion.bind(client))
    .then(function(version) {
      var versionString = [
        version.specifics.majorRelease,
        version.specifics.minorRelease,
        version.specifics.maintenanceRelease
      ].join('.');
      console.log('CC2530/1 firmware version: %s %s', version.type, versionString);
    })
    .then(client.startCoordinator.bind(client))
    .then(client.sysGetExtADDR.bind(client))
    .then(function (mac){
        var mac_v = nu2str(mac[7])+nu2str(mac[6])+nu2str(mac[5])+nu2str(mac[4])+nu2str(mac[3])+nu2str(mac[2])+nu2str(mac[1])+nu2str(mac[0]);
         console.log("mac addr: " + mac_v);
         devices_describe.mac = mac_v;
         devices_describe.PermitJoin=client.sendPermitJoiningRequest.bind(client,{Timeout:120});
    })
    .then(function() {
      var seen = {};
      setInterval(function() {

                  client.devices().then(function(devices) {
                    devices.forEach(function(device) {

                      if (seen[device.IEEEAddress]) {
                        return;
                      }
                         seen[device.IEEEAddress] = true;
                 
                 !function(dev){

                      var device = dev;
                      var setInterval_status = '';
                      var dev_cmd = {};
                      dev_cmd.IEEEAddress = device.IEEEAddress;
                      dev_cmd.shortAddr = device.deviceInfo.shortAddr;
                      
                      console.log('Found', device.toString());
                      
                      device.on('endpoint', function(endpoint) {
                               
                                console.log('Found', endpoint.toString());

                                clearInterval(setInterval_status);

                                endpoint.inClusters().then(function(clusters) {

                                  clusters.forEach(function(cluster) {
                                    console.log('Found', cluster.toString());
                                  });

                                  var onOffCluster = _.findWhere(clusters, {name: 'On/Off'});
                                  
                                   if (onOffCluster) {

                                        console.log('Toggling', onOffCluster.toString());
                                  
                                        dev_cmd.commands= onOffCluster.commands;
                                  
                                         deviceList.push(dev_cmd);   //向设备数组里添加设备
                                     
                                      }
                                });
                      });


                      device.findActiveEndpoints();
                      device.findEndpoints(0x0104, [0x0500], [0x0500]); // HA IAS Zones.
                      
                      
                    setInterval_status = setInterval(function (){
                          device.findActiveEndpoints();
                          device.findEndpoints(0x0104, [0x0500], [0x0500]); // HA IAS Zones.
                      },15000);
                      

                      }(device);//funciton

                    });
                  });
       }, 5000);
    })
    .done();
});



function EnableDevice(mac,Callback){
    enableDevice_Status = 1;
    enableDevice_Callback = Callback;
    for(var i= 0; i<devices_describe.DeviceList.length;i++){

        if(devices_describe.DeviceList[i].IEEEAddress == mac ){
            devices_describe.DeviceList[i].commands.On().done();
        }
    }
}

function setNodeMsgH_callback(callback){
    NodeMsgH_callback=callback;
}


//-----------------------------------------------------------------------------------------
function nu2str(s){
          var q=Math.floor(s/16);
          var x=s%16;
          var i1='';
          var i2='';
           switch(q){
            case  0: i1='0';break;
            case  1: i1='1'; break;
            case  2: i1='2';break;
            case  3: i1='3';break;
            case  4: i1='4';break;
            case  5: i1='5';break;
            case  6: i1='6';break;
            case  7: i1='7';break;
            case  8: i1='8';break;
            case  9: i1='9';break;
            case  10: i1='A';break;
            case  11: i1='B';break;
            case  12: i1='C';break;
            case  13: i1='D';break;
            case  14: i1='E';break;
            case  15: i1='F';break;
           }

           switch(x){
            case  0: i2='0';break;
            case  1: i2='1'; break;
            case  2: i2='2';break;
            case  3: i2='3';break;
            case  4: i2='4';break;
            case  5: i2='5';break;
            case  6: i2='6';break;
            case  7: i2='7';break;
            case  8: i2='8';break;
            case  9: i2='9';break;
            case  10: i2='A';break;
            case  11: i2='B';break;
            case  12: i2='C';break;
            case  13: i2='D';break;
            case  14: i2='E';break;
            case  15: i2='F';break;
           }
  return i1+i2;
}




exports.EnableDevicecmdHandle=EnableDevicecmdHandle;
exports.setNodeMsgH_callback=setNodeMsgH_callback;