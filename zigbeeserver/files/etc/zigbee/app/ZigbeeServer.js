/**
 * Created by Administrator on 2016/8/15.
 */
var mqtt = require('mqtt');
var Zigbee_ZNP = require('./ZIGBEE_ZNP.js');
var loadMqtt=null;
function StartMqtt(){

    loadMqtt = mqtt.connect('mqtt://127.0.0.1:1883');

    /**
     * 连接本地Mqtt服务器成功之后在做响应 订阅消息
     */
    loadMqtt.on('connect' , function (){
        console.log(" Mqtt loadserver  Connect  OK");
        loadMqtt.subscribe('testMessage');
        loadMqtt.subscribe('getGateWayMac');    //订阅ZigbeeServer  返回GateWayMac
        //loadMqtt.subscribe('AddDevice');     //订阅ZigbeeServer  有设备添加到网关
        //loadMqtt.subscribe('DeleteDevices'); //订阅ZigbeeServer  从网关删除设备
        loadMqtt.subscribe('CmdCtr');        //订阅ZigbeeServer  命令控制
        //loadMqtt.subscribe('DevicesEvent');  //订阅ZigbeeServer  Device设备上报状态信息
    });

// loadMqtt.publish('getGateWayMac','getGateWayMac');
    loadMqtt.on('message',function (topic,message,packet){
        console.log("Mqtt  Message: " ,topic,message.toString());


        switch (topic){
            case "CmdCtr":CmdHander(JSON.parse(message.toString())); break;
            case "getGateWayMac": loadMqtt.publish('returnGatewayMac',Zigbee_ZNP.getZigbeeDescribe().GateWayMac); break;
        }

    });
}




function CmdHander(message){
    switch (message.cmd){
        case 'PermitJoin':
            Zigbee_ZNP.PermitJoin(message.timeOut);
            message.skey=0;
            loadMqtt.publish('Return_cmd', JSON.stringify(message));
            break;
//---------------------------------------------------------------------------------
        case 'active':
            console.log('send active message:' ,message);
            Zigbee_ZNP.EnableDevice(message.shortAddr,message.ctl,function (statu){
                if(statu.key == 'ZSuccess') {
                    message.skey = statu.key
                }else{
                    message.skey = statu.key
                }

                loadMqtt.publish('Return_cmd', JSON.stringify(message));
            });
            break;
//---------------------------------------------------------------------------------

        case  'getGateWayMac':
            var data =Zigbee_ZNP.getZigbeeDescribe();
            console.log("GET GateWayMac :",data);
            loadMqtt.publish('returnGatewayMac', data.GateWayMac);
            break;
//----------------------------------------------------------------------------------
        case  'Loop':
            Zigbee_ZNP.pollDevice(message.shortAddr,function(statu){
            if(statu.key == 'ZSuccess') {
                message.skey = statu.key
            }else{
                message.skey = statu.key
            }
              //  console.log('Return_cmd Loop  message :',message);
                loadMqtt.publish('Return_cmd', JSON.stringify(message));
        });

            break;
//----------------------------------------------------------------------------------
        default :
            console.log("No    message:",message);
            break;
    }
}


/**
 * 节点上报事件处理函数
 * @param msg
 * @constructor
 */
function GeteventUpdata(msg){

    console.log("Send  ZIGBEE EVENT : [ ", msg, "]");

    loadMqtt.publish('DeviceEvent', JSON.stringify(msg));

}


/**
 * 新设备加入 事件处理
 */

function AddDevicesEvent(msg){
    console.log("add Devices",msg);
    loadMqtt.publish('AddDevice', JSON.stringify(msg));
}


setTimeout(function startApp(){
    //start MqttServer
    StartMqtt();
    //设置上报事件处理方法
    Zigbee_ZNP.setNodeMsgH_callback(GeteventUpdata);
    //设置节点加入事件处理方法
    Zigbee_ZNP.setNodeAdd_callback(AddDevicesEvent);
},10000);
