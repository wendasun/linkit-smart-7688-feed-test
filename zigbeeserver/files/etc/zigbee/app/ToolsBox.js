/**
 * Created by Administrator on 2016/4/7.
 */

function TenToSixteen(Ten){
    var tmp=Ten.toString(16);
    var Sixteen="00000000" + tmp;
    Sixteen=Sixteen.substr(tmp.length,Sixteen.length);
    return Sixteen;
}

function SixteenToTen(Sixteen){
    return parseInt(Sixteen,16);
}


function num2str(s){
    var q=Math.floor(s/16);
    var x=s%16;
    var i1='';
    var i2='';
    switch(q){
        case  0: i1='0';break;
        case  1: i1='1'; break;
        case  2: i1='2';break;
        case  3: i1='3';break;
        case  4: i1='4';break;
        case  5: i1='5';break;
        case  6: i1='6';break;
        case  7: i1='7';break;
        case  8: i1='8';break;
        case  9: i1='9';break;
        case  10: i1='A';break;
        case  11: i1='B';break;
        case  12: i1='C';break;
        case  13: i1='D';break;
        case  14: i1='E';break;
        case  15: i1='F';break;
    }

    switch(x){
        case  0: i2='0';break;
        case  1: i2='1'; break;
        case  2: i2='2';break;
        case  3: i2='3';break;
        case  4: i2='4';break;
        case  5: i2='5';break;
        case  6: i2='6';break;
        case  7: i2='7';break;
        case  8: i2='8';break;
        case  9: i2='9';break;
        case  10: i2='A';break;
        case  11: i2='B';break;
        case  12: i2='C';break;
        case  13: i2='D';break;
        case  14: i2='E';break;
        case  15: i2='F';break;
    }
    return i1+i2;
}




exports.num2str=num2str;

exports.TenToSixteen=TenToSixteen;
exports.SixteenToTen=SixteenToTen;