'use strict';

var ZCLClient = require('./../lib/zcl/ZCLClient');
var glob = require('glob');
var _ = require('underscore');
var tookit = require('./ToolsBox.js');

var cfg ={};
cfg.panId=0xFFFF;
cfg.ZigbeeFileSave= '/etc/ZigbeeDeviceDb.csv' ;
cfg.NewFindDevices ={};//新发现设备保存表

/**
 * Device 主动发送Event事件 接收处理
 */
cfg.NodeMsgHandle = function (msg){


    if(NodeMsgH_callback != null)
    {
        var event={};
            event.shortAddr =msg.headers.srcAddr;
            event.eventStatus = msg;
            NodeMsgH_callback(event);
    }else{
        console.log(msg);
    }
}


/**
 *
 * 查看AFDataRequest 发送结果
 */
cfg.AFDataRequest = function (msg){
    console.log("AF data return status : [ " + msg.data[0] + " ]");
}

/**
 *有新设备加入事调用处理方法
 */
cfg.sendAddDevice = function (msg){
    if(NodeADD_callback !== null){
        NodeADD_callback(msg);
    }else {
        console.log('New Devices ', msg);
    }
}

var client = new ZCLClient(cfg);


//事件回调方法
var NodeMsgH_callback = null;
//新设备加入回调方法
var NodeADD_callback=null;
//PermitJoin 方法
var PermitJoin_f ;
//设备描述信息
var devices_describe={};


console.log('CC2530/1 test app started!');

glob('{/dev/ttyS1,/dev/ttyS1}', function (err, devices) {

  client.connectToPort(devices[0])
    .then(client.firmwareVersion.bind(client))
    .then(function(version) {
      var versionString = [
        version.specifics.majorRelease,
        version.specifics.minorRelease,
        version.specifics.maintenanceRelease
      ].join('.');
      console.log('CC2530/1 firmware version: %s %s', version.type, versionString);
    })
    .then(client.startCoordinator.bind(client))
    //  .then(client.resetDevice.bind(client,true))
    .then(client.sysGetExtADDR.bind(client))
    .then(function (mac){
        var mac_v = tookit.num2str(mac[7])+tookit.num2str(mac[6])+tookit.num2str(mac[5])+tookit.num2str(mac[4])+tookit.num2str(mac[3])+tookit.num2str(mac[2])+tookit.num2str(mac[1])+tookit.num2str(mac[0]);
         console.log("mac addr: " + mac_v);
         devices_describe.GateWayMac = mac_v;
          //client.sendPermitJoiningRequest({Destination:0,Timeout:120});
          PermitJoin_f=client.sendPermitJoiningRequest.bind(client,{Timeout:120});//{Destination:0,Timeout:120}
    })
      .then(function (){
         // client.Simple_Descriptor('FF32',8);  //获取设备描述信息

         // client.Find_ActiveEndpoints('FF32'); //获取设备Endpoint信息
      })
    .done();
});



//发送激活命令
function EnableDevice(shortAddr,ctl,Callback){

            var buf = new Buffer(0);
            client.sendZCLFrame({
                FrameControl: {
                    ClusterSpecific: true
                },
                DeviceShortAddress: parseInt(shortAddr,16),
                CommandIdentifier: ctl,
                payload :buf
            }, {
                DstAddr: {
                    address: parseInt(shortAddr,16)
                },
                DstEndpoint: 8,
                ClusterID: 6,
                Options: {
                    ackRequest: true,
                    discoverRoute: true
                }
            },Callback);
}


//poll 控制
function pollDevice(shortAddr,Callback){

            var buf = new Buffer(0);
            client.sendZCLFrame({
                FrameControl: {
                    ClusterSpecific: true
                },
                DeviceShortAddress: parseInt(shortAddr,16),
                CommandIdentifier: 15,
                payload :buf
            }, {
                DstAddr: {
                    address: parseInt(shortAddr,16)
                },
                DstEndpoint: 8,
                ClusterID: 6,
                Options: {
                    ackRequest: true,
                    discoverRoute: true
                }
            },Callback);
}


//广播指令
function BroadcastDevice(buffer){

    var buf = buffer || new Buffer(0);
    client.sendZCLFrame({
        FrameControl: {
            ClusterSpecific: true
        },
        //DeviceShortAddress: parseInt(shortAddr,16),
        CommandIdentifier: 15,
        payload :buf
    }, {
        DstAddr: {
            address: 0XFFFF
        },
        DstEndpoint: 8,
        ClusterID: 6,
        Options: {
          //  ackRequest: true,
            discoverRoute: true
        }
    },function(msg){
        console.log("Broadcast request status  : " + msg);
    });
}


//设置事件响应
function setNodeMsgH_callback(callback){
    NodeMsgH_callback=callback;
}
//新设备加入

function setNodeAdd_callback(callback){
    NodeADD_callback=callback;
}

//返回Zigbee设备描述信息

function getZigbeeDescribe(){
    return devices_describe;
}

//PermitJoin 命令
function PermitJoin(){
    PermitJoin_f();
}

//删除节点命令
function RemoveDeviceMac(Mac){
    client.RemoveDevice(Mac,Mac.shortAddr);
}


//-----------------------------------------------------------------------------------------



exports.EnableDevice=EnableDevice;//激活控制
exports.setNodeMsgH_callback=setNodeMsgH_callback;//主动上报事件处理
exports.getZigbeeDescribe=getZigbeeDescribe;//获取server节点描述
exports.PermitJoin=PermitJoin;//开启配对模式120秒
exports.pollDevice=pollDevice;//pollDevice
exports.RemoveDeviceMac=RemoveDeviceMac;//删除节点
exports.BroadcastDevice=BroadcastDevice;//广播指令
exports.setNodeAdd_callback=setNodeAdd_callback;//新设备加入回调方法
