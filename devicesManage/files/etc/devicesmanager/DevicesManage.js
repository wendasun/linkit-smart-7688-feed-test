/**
 * Created by Administrator on 2016/8/12.
 */

var HandleMessage = require('./HandleMessage.js');
var WifiManage = require('./WifiManage.js');


var WifiHandle = new WifiManage();
var MqttHandle = new HandleMessage();


    MqttHandle.on('NetMqttConnectClose',function (WifiDevices){
            WifiHandle.WifiReconnect(WifiDevices);
    })

    MqttHandle.on('wifiscan',function (wifiscanString){
        WifiHandle.WifiScan().then(function (wifiscanString){
            var arraywifi=wifiscanString.split("\n");
            var wifiList = {wifilist:arraywifi};
            MqttHandle.emit("wifilist_response",wifiList);
        })
    })


    MqttHandle.on('DevicesFileInit',function (){
        WifiHandle.WifiInit();
    })

    MqttHandle.on('SetWifiInfo',function (WifiInfo){
        console.log("Set Wifi Info -> SSID:",WifiInfo.SSID,"  Key:",WifiInfo.Key);
        WifiHandle.ChangeWifi(WifiInfo.SSID,WifiInfo.Key);
    })