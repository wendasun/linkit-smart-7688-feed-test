/**
 * Created by Administrator on 2016/9/6.
 */
var exec = require('child_process').exec;
var ExecFile= require('child_process');
var EventEmitter = require('events').EventEmitter;
var  Promise = require('bluebird');
var util = require('util');


function WifiMange(){
    //DevicesFile 管理对象
    EventEmitter.call(this);
}

util.inherits(WifiMange,EventEmitter);


//设备第一次上电调用方法
WifiMange.prototype.WifiInit=function (){
    ExecFile.execFile("/bin/connect2ap",["nbridge_safe","20167772703"],null,function(error,stdout,stderr){
        if (error !== null) {
            console.log('Init  WIFI: Error ' + error);
            console.log('Init  wIFI stderr: ' + stderr);
        }else{
            console.log('Init Wifi Success');
        }
    });

}


//设备第一次上电调用方法
WifiMange.prototype.ChangeWifi=function (SSID,Key){

    console.log("Change Wifi -->   SSID:",SSID,"  Key:",Key);
    ExecFile.execFile("/bin/connect2ap",[SSID,Key],null,function(error,stdout,stderr){
        if (error !== null) {
            console.log('Change  WIFI: Error ' + error);
            console.log('Change  wIFI stderr: ' + stderr);
        }else{
            console.log('Change Wifi Success');
        }
    });
}

//wifi设置
WifiMange.prototype.WifiReconnect=function (WifiDevices){
    var self = this;
    //查找当前wifi ssid
    new Promise(function (resolve,reject){
        var CmdSiteSurvey = 'uci get wireless.sta.ssid';
        exec(CmdSiteSurvey,function(error,stdout,stderr){
            if (error !== null) {
                console.log('SiteSurvey WIFI: Error ' + error);
                console.log('SiteSurvey stderr: ' + stderr);
            }else{
                resolve(stdout);
            }
        });
    }).then(function (SSID){
            console.log("Wifi File Sava WIFI  SSID : ",SSID)
            self.WifiScan().then(function (wifiscanString){
                for(var i=0; i<WifiDevices.length;i++) {
                    var wifiName = wifiscanString.match(WifiDevices[i].SSID);
                    if (wifiName !== null) {
                        var fwFile = SSID.match(WifiDevices[i].SSID);
                           if(fwFile == null) {
                               self.ChangeWifi(WifiDevices[i].SSID, WifiDevices[i].Key);
                           }
                        break;
                    } else {
                        console.log("Could not find specified wifi Name:[",WifiDevices[i].SSID,"]");
                    }
                }
            })
        })

}



//扫描当前设备有那些wifi
WifiMange.prototype.WifiScan=function (){
 return  new Promise(function (resolve,reject){
        var CmdSiteSurvey = 'iwpriv ra0 set SiteSurvey=0';
        exec(CmdSiteSurvey,function(error,stdout,stderr){

            if (error !== null) {
                console.log('SiteSurvey WIFI: Error ' + error);
                console.log('SiteSurvey stderr: ' + stderr);
            }else{

                setTimeout(function (){
                    var GetSiteSurveyList =  'iwpriv ra0 get_site_survey |  awk -F\' \' \'{print $2}\'';
                    exec(GetSiteSurveyList,function(error,stdout,stderr){

                        if (error !== null) {
                            console.log('GetSiteSurveyList Error: ' + error);
                            console.log('GetSiteSurveyList stderr: ' + stderr);
                        }else{
                            console.log('GetSiteSurveyList stdout: ' + stdout);
                            resolve(stdout);
                        }
                    })
                },6000)
            }
        });
    })
}

module.exports = WifiMange;