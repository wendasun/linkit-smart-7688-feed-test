/**
 * Created by Administrator on 2016/9/2.
 */
var util = require('util');
var EventEmitter = require('events').EventEmitter;
var NetServerMqtt=require('./NetServerMqtt.js');
var LoadServerMqtt=require('./LoadServerMqtt.js');
var DevicesFileCtr = require('./DevicesFileCtr.js');
var httpRequest = require('./httpRequest.js');
var  Promise = require('bluebird');
var exec = require('child_process').exec;


var ConncetZigbeeServerSetInterval = null;


function HandleMessage(FileCTR){
    //DevicesFile 管理对象
    EventEmitter.call(this);

    this.FileC= new DevicesFileCtr({FileSavePath:'/etc/ZigbeeDeviceDb.csv',
                                    DevicesInfoFilePath:'/etc/DevicesInfo.cfg',
                                    RegKeyFilePath:'/etc/RegKeyInfo.cfg',
                                    WifiMacADDRFilePath:'/sys/class/net/ra0/address'
                                    });
    this.LoadMqtt= new LoadServerMqtt();
    this.LoadMqtt.on('LoadServerMqttMessage',this.LoadMqttHandleMessage.bind(this));
    this.LoadMqtt.on('LoadServerConnectSuccess',this.ConncetZigbeeServer.bind(this));

    this.on("wifilist_response",this.WifiListResponse.bind(this))
}

//使用util工具包的inherits实现继承
util.inherits(HandleMessage,EventEmitter);

/**
 * 初始化完成 后连接远程MqttServer
 */

HandleMessage.prototype.WifiListResponse = function (wifilist){
    wifilist.cmd = "wifilist_response";
    this.NetMqtt.NetserverClient.publish('$me/state', JSON.stringify(wifilist));
}

var ServerMqttNumber=0;
HandleMessage.prototype.ConnectNetServerMqtt = function (){
    if(ServerMqttNumber == 0) {
        ServerMqttNumber=1;
        this.NetMqtt = new NetServerMqtt(this.FileC.DevicesDescribe);
        this.NetMqtt.on('NetServerMqttMessage', this.NetMqttHandleMessage.bind(this));
        this.NetMqtt.on('NetServerConnectSuccess', this.NetMqttConnectSuccess.bind(this));
        this.NetMqtt.on('NetMqttConnectClose', this.NetMqttConnectClose.bind(this))
    }
}

HandleMessage.prototype.NetMqttConnectClose = function (){
    this.emit('NetMqttConnectClose',this.FileC.DevicesDescribe.WifiDevices);
}


//NetMqttServer 连接成功之后处理接口
HandleMessage.prototype.NetMqttConnectSuccess = function (){
    console.log('[HandleMessage]','NetMqttConnectSuccess!');

    //this.PollDevicesConncet();

    //使本地设备与服务器设备同步
    this.getListDevicesChild();//获取当前云服务器设备列表并上线拥有的设备

    this.ConnectWifiSSID();//上报wifi的连接信息
}


HandleMessage.prototype.ConnectWifiSSID = function (){
    var self = this;
    var CmdSiteSurvey = 'uci get wireless.sta.ssid';
    exec(CmdSiteSurvey,function(error,stdout,stderr){
        if (error !== null) {
            console.log('SiteSurvey WIFI: Error ' + error);
            console.log('SiteSurvey stderr: ' + stderr);
        }else{
            var ssid=stdout.split("\n");
            self.NetMqtt.NetserverClient.publish('$me/state', JSON.stringify({cmd:"wifissid_response",WifiSSID:ssid[0]}));
        }
    });
}


HandleMessage.prototype.getListDevicesChild = function (){
    var self = this;
    var url_getListDevices = {
        hostname: 'api.amber-link.com',
        path: '/v1/dev/' + this.FileC.DevicesDescribe.GateWayUid + '/info',
        method: 'GET',
        headers:{
            Authorization:this.FileC.DevicesDescribe.token,
            'Content-Type':'application/json'
        }
    };

    var getListDevices_http = new httpRequest(url_getListDevices);
    getListDevices_http.runPost(function (msg){
        console.log("NetServer get Child Devices List :",msg.result.child);


        var childArray = msg.result.child;
        var devicesArray = [];

        for(var i=0;i < childArray.length;i++){
            for(var j=0;j<self.FileC.DevicesDescribe.devices.length;j++){
                if(childArray[i] == self.FileC.DevicesDescribe.devices[j].uid){
                    devicesArray.push(self.FileC.DevicesDescribe.devices[j]);
                }
            }
        }

        self.FileC.DevicesDescribe.devices = devicesArray;
        self.FileC.updataDevice();

        for(var i=0;i<self.FileC.DevicesDescribe.devices.length;i++) {
            var cnnt = {};
            cnnt[self.FileC.DevicesDescribe.devices[i].uid] = true;
            self.NetMqtt.NetserverClient.publish('$me/child_cnnt', JSON.stringify(cnnt));
        }

        console.log(self.FileC.DevicesDescribe);
    });
}

HandleMessage.prototype.ConncetZigbeeServer = function (){
//判断ZigbeeServer.js 是否开始工作
    var self=this;
    ConncetZigbeeServerSetInterval = setInterval(function (){
        console.log("Conncet to ZigbeeServer ing ...");
        self.LoadMqtt.LoadserverClient.publish('getGateWayMac','getGateWayMac'); //像MqttServer获取Mac 成后续注册流程
    },5000);
}



//初始化HandleMessage对象的功能
HandleMessage.prototype.initHandleMessage = function (message){
    clearInterval(ConncetZigbeeServerSetInterval);  //停止判断ZigbeeServer是否开始工作
    console.log("DevicesManage To ZigbeeServer Connect Success.");
    var self = this;
    console.log('[Handle Message]    initHandleMessage ');
    if(this.FileC.getDeviceListFile() != 'SUCCEED'){
        //初始化wifi
        this.emit('DevicesFileInit');
        console.log('[Handle Message] No Find <Zigbee_File>   init FileDevices');
        //_conect 如果当前网络连接出错  则三秒钟后重新执行_connect()
        !function _conect() {
            var connect = new httpRequest({hostname: '120.24.174.226', path: '/time', method: 'GET'});
            connect.runPost(function (data) {
                if (data != 'NoConnect') {
                    //获取网关设备信息
                    if(self.FileC.getDevicesInfoFile() == 'SUCCEED') {
                        self.InitFileDevices(message); //像MqttServer获取Mac 成后续注册流程
                    }else{
                        console.log('Dont Find DevicesInfo File... path:/etc/DevicesInfo.cfg');
                        self.createDevicesInfoFile(function (){
                            _conect();
                        })
                    }
                }else{
                    console.log("Failed to register network connection for the first time: HandMessage.js + 36");
                    setTimeout(function (){_conect();},5000)
                }})}();
    }else{
        this.ConnectNetServerMqtt();
    }
}

HandleMessage.prototype.createDevicesInfoFile = function (callBacke){
    var self=this;
    var RegKeyInfo = this.FileC.getRegkey();
    if(RegKeyInfo !== 'DEFEAT'){
        var url={hostname: 'api.amber-link.com',
            path: '/v1/register/dev?regkey='+RegKeyInfo.regkey+'&mac='+RegKeyInfo.WifiMac,
            method: 'GET'};
        console.log("get DevicesInfo URL:",url);
    var getDevicesInfo = new httpRequest(url);
        getDevicesInfo.runPost(function (data){
            console.log(data);
            self.FileC.SaveDevicesInfoFile(data.result.info);
            callBacke();
        });
    }
}


HandleMessage.prototype.LoadMqttHandleMessage = function (topic,message,packet){
    switch (topic){
        case 'returnGatewayMac': this.initHandleMessage(message);    break;
        case 'AddDevice': this.JudgeNewDevice(message);        break;
        case 'Return_cmd': this.LoadMqttReturn_cmd(message);       break;
        case 'DeviceEvent': this.LoadMqttDeviceEvent(message);       break;
        default :       break;
    }
}


HandleMessage.prototype.NetMqttHandleMessage = function (topic,message,packet){
    try {var msg = JSON.parse(message.toString());}
    catch(err) {msg = 'JSON Parser Error: ' + msg;}

    if(msg.cmd) {
        switch (msg.cmd) {
            case "PermitJoin":
                this.LoadMqtt.LoadserverClient.publish('CmdCtr', '{"cmd":"PermitJoin"}');
                break;
            case "WifiInfo":
                this.SetWifiInfo(msg.SSID,msg.Key);
                break;
            case "active":
                this.ActiveDevices(msg.parameter);
                break;
            case "wifiscan":
                this.emit("wifiscan");
                break;
            default :
                break;
        }
    }
}

HandleMessage.prototype.ActiveDevices = function (parameter){
    for(var i=0;i<this.FileC.DevicesDescribe.devices.length;i++){
        if(parameter.uid == this.FileC.DevicesDescribe.devices[i].uid){
            var data  = {cmd:'active',
                IEEEAddress:this.FileC.DevicesDescribe.devices[i].IEEEAddress,
                shortAddr:this.FileC.DevicesDescribe.devices[i].shortAddr,
                ctl:parameter.ctl,
                uid:parameter.uid};
            this.LoadMqtt.LoadserverClient.publish('CmdCtr',JSON.stringify(data));
        }
    }
}


HandleMessage.prototype.SetWifiInfo = function (SSID,Key){
 var wifiinfo={
        SSID:SSID,
        Key:Key
 }

    this.NetMqtt.NetserverClient.publish('$me/state', JSON.stringify({cmd:"SetWifi_Response",Status:"Success"}));

    this.emit("SetWifiInfo",wifiinfo);

    for(var i=0; i<this.FileC.DevicesDescribe.WifiDevices.length;i++){
        if(this.FileC.DevicesDescribe.WifiDevices[i].SSID == SSID){
            if(this.FileC.DevicesDescribe.WifiDevices[i].Key !== Key){
                this.FileC.DevicesDescribe.WifiDevices[i].Key = Key;
                this.FileC.updataDevice(); //将这个数组保存起来
            }
            return
        }
    }

    if(i == this.FileC.DevicesDescribe.WifiDevices.length){
        this.FileC.DevicesDescribe.WifiDevices.push(wifiinfo);
        this.FileC.updataDevice(); //将这个数组保存起来
    }


}


HandleMessage.prototype.LoadMqttDeviceEvent = function (message){
    try {var msg = JSON.parse(message.toString());}
    catch(err) {msg = 'JSON Parser Error: ' + msg;}

    for(var i=0; i< this.FileC.DevicesDescribe.devices.length;i++){
        if(msg.shortAddr == parseInt(this.FileC.DevicesDescribe.devices[i].shortAddr, 16)){
            var data=null;

            if(msg.eventStatus.model == 'Yongmin') {
                var str = msg.eventStatus.headers.data.data[3].toString(2);
                 data = {
                    Alarm1: str[5],
                    Alarm2: str[4],
                    Battery: str[2],//0
                    LinkQ: msg.eventStatus.headers.linkQuality,
                    version: 1
                }
            }else if(msg.eventStatus.model == 'Mi'){
                var str = msg.eventStatus.headers.data.data[6].toString();
                 data = {
                    Alarm1: str,
                    Alarm2: 0,
                    Battery:0,//0
                    LinkQ: msg.eventStatus.headers.linkQuality,
                    version: 1
                }
            }else if(msg.eventStatus.model == 'MK'){
                var str = msg.eventStatus.headers.data.data[6];
                var status='null';
                switch (str){
                    case 200:
                        status = 'attach';
                        break;
                    case 201:
                        status = 'detach';
                        break;

                    case 204:
                        status = 'active';
                        break;

                    case 205:
                        status = 'standby';
                        break;
                    default:
                        status = 'null';
                        break;
                }
                data = {
                    model: 'MK',
                    status: status,
                    LinkQ: msg.eventStatus.headers.linkQuality,
                    version: 1
                }
            }

            console.log("Load Send To Net:",data);
            this.NetMqtt.NetserverClient.publish('$me/child/' + this.FileC.DevicesDescribe.devices[i].uid + '/state', JSON.stringify(data));
            break;
        }
    }
}


HandleMessage.prototype.LoadMqttReturn_cmd = function (message){
    var msg  =JSON.parse(message);

    switch (msg.cmd){
        case 'Loop':

            for(var i=0;i<this.FileC.DevicesDescribe.devices.length;i++){
                if(msg.shortAddr == this.FileC.DevicesDescribe.devices[i].shortAddr){
                    var status = false;
                    if(msg.skey == 'ZSuccess'){
                        status = true;
                    }
                    if(this.FileC.DevicesDescribe.devices[i].Conncet != status){
                        this.FileC.DevicesDescribe.devices[i].Conncet = status;
                        var cnnt = {};
                        cnnt[this.FileC.DevicesDescribe.devices[i].uid] = status;
                        this.NetMqtt.NetserverClient.publish('$me/child_cnnt',JSON.stringify(cnnt));
                    }
                    break;
                }
            }
            break;
        default  :
            console.log("No heandle cmdctr...",msg);
            break

    }

}

HandleMessage.prototype.JudgeNewDevice  = function (message){
    var msg  =JSON.parse(message.toString());
    for(var i=0;i < this.FileC.DevicesDescribe.devices.length;i++){
        if(msg.IEEEAddress == this.FileC.DevicesDescribe.devices[i].IEEEAddress){
            if(msg.shortAddr !== this.FileC.DevicesDescribe.devices[i].shortAddr){
                this.FileC.DevicesDescribe.devices[i].shortAddr = msg.shortAddr;
                this.FileC.updataDevice();
            }
            console.log("This equipment to exist within the gateway :",msg);
            break;
        }
    }

    if(i == this.FileC.DevicesDescribe.devices.length){
        this.AddDevicesRegister(message);
    }
};


HandleMessage.prototype.AddDevicesRegister = function (message){
    var msg  =JSON.parse(message.toString());
    var self=this;
    new Promise(function (resolve,reject){
        var url_QueryDevice = {
            hostname: 'api.amber-link.com',
            path: '/v1/util/query?mac=' +msg.IEEEAddress,
            method: 'GET',
            headers:{
                Authorization:self.FileC.DevicesDescribe.token,
                'Content-Type':'application/json'
            }
        };

        var QueryDevice_http = new httpRequest(url_QueryDevice);

        QueryDevice_http.runPost(function (Data){
            if(Data.err == null){
                console.log("ADD New Devices  Find Server device_id...[Yes]");
                msg.DevicesId=Data.result.device_id; //保存DevicesId
                resolve(Data.result.device_id);
            }else{
                console.log("ADD New Devices  Find Server device_id...[No]");
                console.log(Data);
                resolve();
            }
        });

    }).then(function (devices_id){
            var DEVID= 'mac=' +msg.IEEEAddress;
            if(devices_id){
                DEVID='device_id=' + devices_id;
            }



            console.log("Find New Devices ",msg);

            var DevicesMode={
                0x01:'Mi',//小米设备
                0x02:'MLW0002TX',//无线充电
                0x15:'UME0002CN', //门磁设备
                0x2b:'UME0001CN'     //烟雾设备
            };
            var ZoneType='unknown';
            var model = '';

            if(msg.typeZone){
                if(msg.typeZone != 0x01) {
                    ZoneType = DevicesMode[msg.typeZone];
                    model = '&model=' + ZoneType
                }
            }

            var url_getUid = {
                hostname: 'api.amber-link.com',
                path: '/v1/register/dev?' + DEVID + model,
                method: 'GET'
            };

            console.log("URL :",url_getUid);
            var uid_http = new httpRequest(url_getUid);

            uid_http.runPost(function (Data) {
                if (Data !== 'NoConnect' && Data !== null) {
                    msg.uid = Data.result.uid;
                    msg.status = 1;
                    msg.Register = 'success';
                    msg.Conncet = false;
                } else {
                    msg.uid = "No_GET_UID";
                    msg.status = 0;
                    msg.Register = 'fail';
                }

                if (msg !== 'No_GET_UID'){
                    //add child devices
                    var url_addChild = {
                        hostname: 'api.amber-link.com',
                        path: '/v1/dev/' + self.FileC.DevicesDescribe.GateWayUid + '/child/list',
                        method: 'POST',
                        headers:{
                            Authorization:self.FileC.DevicesDescribe.token,
                            'Content-Type':'application/json'
                        }
                    };

                    var PostData = {};
                    PostData[Data.result.uid] = 1;

                    var addChild_http = new httpRequest(url_addChild, PostData);
                    addChild_http.runPost(function (addGroupmsg) {
                        console.log("Add Devices Group Status",addGroupmsg);
                        self.FileC.addDevice(msg);
                        //加入设备后设备直接上线
                        var cnnt = {};
                        cnnt[msg.uid] = true;
                        self.NetMqtt.NetserverClient.publish('$me/child_cnnt', JSON.stringify(cnnt));
                    })
                }else{
                    self.FileC.addDevice(msg);
                }
            })
        })



}



HandleMessage.prototype.InitFileDevices = function (message){
    var self = this;
    console.log('Topic returnGatewayMac Mac: ' + message.toString());
    self.FileC.DevicesDescribe.GateWayMac = message.toString();
    new Promise(function (resolve,reject){
        function _regGateWay(uid,token){
            if(uid == 'error')
            {
                console.log("get GateWay Uid and Token err  reconnect !!! 3s");
                setTimeout(function (){self._register_GateWay(_regGateWay);},3000);
            } else {
                var UidToken = {uid: uid, token: token};
                resolve(UidToken);}
        }
        self._register_GateWay(_regGateWay);
    }).then(function (UidToken){
            self.FileC.DevicesDescribe.token = UidToken.token;
            self.FileC.DevicesDescribe.GateWayUid = UidToken.uid;
            self.FileC.DevicesDescribe.devices = new Array();
            self.FileC.DevicesDescribe.NoDevices = new Array();
            self.FileC.DevicesDescribe.WifiDevices = [{SSID:"nbridge_safe",Key:"20167772703"}];
            self.FileC.updataDevice(); //将这个数组保存起来
            self.ConnectNetServerMqtt();
        });
}



/**
 * @获取到了网关的MAC地址 然后向服务器校验此MAC地址是否是真实的   服务器会返回 token 和 uid
 * @data {device_id:‘xxxxx’, [device_key:'xxxx', group:'xxxxxx']}
 */
HandleMessage.prototype._register_GateWay = function (callback){
    var DevicesId= this.FileC.DevicesDescribe.DeviceId;
    var GateWayMac=this.FileC.DevicesDescribe.GateWayMac;
    var url_getUid = {
        hostname: 'api.amber-link.com',
        path: '/v1/register/dev?device_id=' + DevicesId +'&model=GateWay&psk='+GateWayMac+'&name='+this.FileC.DevicesDescribe.serial, //"&group=OCN_DEV"
        method: 'GET'
    };

    var uid_http = new httpRequest(url_getUid);
    uid_http.runPost(function (Data){
        if(Data == null || Data == 'NoConnect'){
            callback('error');
            return ;
        }
        console.log("gateWay Uid :"+ Data.result.uid);
        console.log("gateWay Uid :"+ Data.result.token);
        callback(Data.result.uid,Data.result.token);
    })
};



/**
 * 删除记录在云端的子节点
 */
HandleMessage.prototype.DeleteDevicesChild = function (deviesUid){

    var url_DeleteDevices = {
        hostname: 'api.amber-link.com',
        path: '/v1/dev/' + this.FileC.DevicesDescribe.GateWayUid + '/child',
        method: 'POST',
        headers:{
            Authorization:this.FileC.DevicesDescribe.token,
            'Content-Type':'application/json'
        }
    };

    var postData={};
    postData[deviesUid]=null;


    var DeleteDevices_http = new httpRequest(url_DeleteDevices,postData);
    DeleteDevices_http.runPost(function (msg){
        console.log("<<<",msg);
    });
}



var connectnum = 0;
HandleMessage.prototype.PollDevicesConncet= function (){
    var self = this;
    setInterval( function _pollDevicesConncet(){
        if(self.FileC.DevicesDescribe.devices.length !== 0) {
            var cmd = {};
            cmd.shortAddr = self.FileC.DevicesDescribe.devices[connectnum].shortAddr;
            cmd.cmd = 'Loop';
            //console.log("PollDevicesConnect  : ",cmd);
            if(self.FileC.DevicesDescribe.devices[connectnum].EndpointIds && self.FileC.DevicesDescribe.devices[connectnum].EndpointIds[0]==8) {
                self.LoadMqtt.LoadserverClient.publish('CmdCtr', JSON.stringify(cmd));
            }
            if (++connectnum == self.FileC.DevicesDescribe.devices.length) {
                connectnum = 0;
            }
        }
    },3000);
}


module.exports = HandleMessage;