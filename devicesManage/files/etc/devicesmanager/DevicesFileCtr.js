/**
 * Created by Administrator on 2016/8/10.
 */

var fs = require('fs');


/**
 * File主控制对象
 *
 * config: {{
 *          FileLoadURL:'xxxx'       //将ZIGBEE文件保存在什么位置
 *          DevicesInfoFilePath:'xxx'    //需要读取网关自身信息的文件
 *}}
 *
 */

function DeviceDocumentControl(cfg){
    this.config=cfg;
    this.DevicesDescribe = {}; //用来保存Device设备的数组
}


/**
 *读取Devices List File 中已经有的device信息
 * {
 * token：'XXXXXX',
 * GateWayUid:'XXXX',
 * DeviceId:'xxxx',
 * GateWayMac:'XXXX',
 * devices:[{uid:'xxxx',mac:'xxxxx',shortaddr:'xxxxx',status:1},
 *          {uid:'xxxx',Mac:'xxxx',shortid:'xxxx'}]
 *
 * NoDevices:[{Mac:'xxxx',shortid:'xxxx'},
 *            {Mac:'xxxx',shortid:'xxxx'}],
 *
 * }
 */
DeviceDocumentControl.prototype.getDeviceListFile = function() {
    var status="";
    var  exists  = fs.existsSync(this.config.FileSavePath);
    if(exists){
        var data = fs.readFileSync(this.config.FileSavePath,'utf8');
        this.DevicesDescribe = JSON.parse(data);
        status = "SUCCEED";
    }else{
        status = "DEFEAT";
    }
    return  status;
};

DeviceDocumentControl.prototype.getRegkey = function (){
    var status="";
    var  exists  = fs.existsSync(this.config.RegKeyFilePath);
    if(exists){
        var data = fs.readFileSync(this.config.RegKeyFilePath,'utf8');
        var RegKeyInfo= JSON.parse(data);

        var wifimac = fs.readFileSync(this.config.WifiMacADDRFilePath,'utf8');
        RegKeyInfo.WifiMac= wifimac.split('\n')[0];
        var status=RegKeyInfo;
    }else{
        status = "DEFEAT";
    }
    return  status;
}

DeviceDocumentControl.prototype.SaveDevicesInfoFile = function (msg){
  try {
      var str = JSON.stringify(msg);
  }catch(e){
      console.log("err Save DevicesInfoFile --->",e);
  }

    fs.writeFileSync(this.config.DevicesInfoFilePath, str);
    console.log("FileCtr: Save DevicesInfoFile  [ ok ]");
}


DeviceDocumentControl.prototype.getDevicesInfoFile = function() {
    var status="";
    var  exists  = fs.existsSync(this.config.DevicesInfoFilePath);
    if(exists){
        var data = fs.readFileSync(this.config.DevicesInfoFilePath,'utf8');
        var DevicesInfo= JSON.parse(data);
        this.DevicesDescribe.DeviceId=DevicesInfo.device_id;
        this.DevicesDescribe.serial=DevicesInfo.serial;
        status = "SUCCEED";
    }else{
        status = "DEFEAT";
    }
    return  status;
}


/**
 * @param Mac     需要删除设备的Mac地址
 * @param uid     需要删除设备的Uid地址
 */
DeviceDocumentControl.prototype.deleteDevice = function(Mac,uid){
    fs.writeFileSync(this.config.FileSavePath, 'data');
};

/**
 *
 * @param DeicesJson 传入加入文件的Device设备信息
 */
DeviceDocumentControl.prototype.addDevice = function(DevicesJson){
    var i=0;

    if(DevicesJson.Register == 'success') {  //注册成功的设备
        for (i = 0; i < this.DevicesDescribe.devices.length; i++) {
            if (this.DevicesDescribe.devices[i].IEEEAddress == DevicesJson.IEEEAddress) {
                if (this.DevicesDescribe.devices[i].shortAddr != DevicesJson.shortAddr) {
                    this.DevicesDescribe.devices[i].shortAddr = DevicesJson.shortAddr;
                    return this.updataDevice();
                }
                break;
            }
        }

        if(i == this.DevicesDescribe.devices.length){
            this.DevicesDescribe.devices.push(DevicesJson);
            return this.updataDevice();
        }

    }else { //注册失败的设备（非法的设备）
        for (i = 0; i < this.DevicesDescribe.NoDevices.length; i++) {
            if (this.DevicesDescribe.NoDevices[i].IEEEAddress == DevicesJson.IEEEAddress) {
                if (this.DevicesDescribe.NoDevices[i].shortAddr != DevicesJson.shortAddr) {
                    this.DevicesDescribe.NoDevices[i].shortAddr = DevicesJson.shortAddr;
                    return this.updataDevice();
                }
                break;
            }
        }

        if(i == this.DevicesDescribe.NoDevices.length){
            this.DevicesDescribe.NoDevices.push(DevicesJson);
            return this.updataDevice();
        }
    }
   // fs.appendFileSync(this.config.FileSavePath, data, [options])
};

/**
 * @param DevicesJson 如果有节点信息状态更新调用此方法  例如:短地址改变
 */
DeviceDocumentControl.prototype.updataDevice = function(){
      var DevicesString=JSON.stringify(this.DevicesDescribe);
       fs.writeFileSync(this.config.FileSavePath, DevicesString);
      console.log("FileCtr: UpdataDevice  [ ok ]");
};



module.exports = DeviceDocumentControl;