/**
 * Created by Administrator on 2016/9/2.
 */
var util = require('util');
var EventEmitter = require('events').EventEmitter;
var  Promise = require('bluebird');
var mqtt = require('mqtt');


var Url_ServerMqtt='mqtt://mq.amber-link.com';


function startNetServerMqtt(connectMsg){
    //本函数继承EventEmitter必要步骤
    var self = this;
    EventEmitter.call(this);
    this.NetserverClient = mqtt.connect(Url_ServerMqtt,{password:connectMsg.token,
        reconnectPeriod:1000*10});
    /**
     * 连接互联网Mqtt服务器成功之后在做响应  订阅消息
     */
    this.NetserverClient.on('connect', function (){
        console.log('mqttServer Connect OK!');
        self.SeverOnsubscribe();
    });

    this.NetserverClient.on('reconnect', function () {
        console.log('Trying reconnected to MQ Srv - '+ 'mqtt://amber-link.com' + ' ...');
    });

    this.NetserverClient.on('offline', function () {
        console.log('Offline to MQ Srv - ');
    });

    this.NetserverClient.on('error', function (error) {
        console.log('error to MQ Srv - ', error);
    });

    this.NetserverClient.on('close', function (error) {
        console.log('close to MQ Srv - ', error);
        self.emit('NetMqttConnectClose');
    });

    this. NetserverClient.on('message',function (topic,message,packet){
        try {msg = JSON.parse(message.toString());}
        catch(err) {msg = 'JSON Parser Error: ' + msg;}
        console.log("Net Send To Load:",msg);

        self.emit('NetServerMqttMessage',topic,message,packet);
    });

}
//使用util工具包的inherits实现继承
util.inherits(startNetServerMqtt,EventEmitter);

startNetServerMqtt.prototype.SeverOnsubscribe=function (){
    this.emit('NetServerConnectSuccess');
}

module.exports = startNetServerMqtt;