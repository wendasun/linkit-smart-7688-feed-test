/**
 * Created by Administrator on 2016/8/15.
 */
var http = require("http");

/*封装了一个request 的方法
 * optionUrl  : 地址对象     {
                             hostname : "120.24.174.226",
                             port : 80,
                             path : "/node_v2",
                             method : "POST",
                             };
 *postData  : 要想Server发送的数据
 * */
function httpRequest(optionsUrl,postData) {

    this.optionsUrl=optionsUrl || null;
    this.postData=postData || null;

    //getJson : 方法执行后会将获取到的数据传入此回调方法
    this.runPost = function(getJson) {
        var req=null;

        var request_timer = setTimeout(function() {
            req.abort();
            console.log(' HTTP Request Timeout. -->>');
        }, 5000);

        var ObjJson = null;
        req = http.request(this.optionsUrl, function(res) {
            clearTimeout(request_timer);

            var response_timer = setTimeout(function() {
                res.destroy();
                console.log(' HTTP Response Timeout. <<--');
            }, 30000);

            console.log('post http Status :[' + res.statusCode +']');
            // console.log('headers :' + JSON.stringify(res.headers));

            res.on('data', function(chunk) {

                try {
                    ObjJson = JSON.parse(chunk);
                    // console.log(ObjJson);

                    if(ObjJson.err !== null){
                        console.log("http request Error" + ObjJson);
                    }

                }catch (err) {
                    console.log("Json.Parse Error : " + err);
                }

            });

            res.on('end', function() {
                clearTimeout(response_timer);
                console.log('Post Execute success');
                getJson(ObjJson);
            });

        });

        req.on('error', function(e) {
            console.log("http Request Error : httpRequest.js + 48  !" +e);
            getJson("NoConnect");
        });

        req.setTimeout(10000);
      /*
      req.setTimeout(10000,function(){
            console.log("HTTP Request TimeOut !");
            getJson("NoConnect");
        });
        */


        if(optionsUrl.method == 'POST') {
            var str = JSON.stringify(this.postData);
            req.write(str);
        }
        req.end();
    }

}

module.exports = httpRequest;