/**
 * Created by Administrator on 2016/9/2.
 */
var util = require('util');
var EventEmitter = require('events').EventEmitter;
var  Promise = require('bluebird');
var mqtt = require('mqtt');


var Url_ServerMqtt='mqtt://127.0.0.1:1883';


function startLoadServerMqtt(connectMsg){
    //本函数继承EventEmitter必要步骤
    var self = this;
    EventEmitter.call(this);


    this.LoadserverClient = mqtt.connect(Url_ServerMqtt);

    this.LoadserverClient.on('connect', function (){
        console.log('mqttServer Connect OK!');
        self.SeverOnsubscribe();
    });

    this.LoadserverClient.on('reconnect', function () {
        console.log('Trying reconnected to MQ Srv - '+ 'mqtt://loadhost.com' + ' ...');
    });

    this.LoadserverClient.on('offline', function () {
        console.log('Offline to MQ Srv - ');
    });

    this.LoadserverClient.on('close', function (error) {
        console.log('close to MQ Srv - ', error);
    });

    this.LoadserverClient.on('error', function (error) {
        console.log('error to MQ Srv - ', error);
    });

    this.LoadserverClient.on('message',function (topic,message,packet){

        try {msg = JSON.parse(message.toString());}
        catch(err) {msg = 'JSON Parser Error: ' + err;}
        console.log("ZigbeeServer Send To Devices-Manage"," topic :["+ topic +"]");
        self.emit('LoadServerMqttMessage',topic,message,packet);
    });



}
//使用util工具包的inherits实现继承
util.inherits(startLoadServerMqtt,EventEmitter);


startLoadServerMqtt.prototype.SeverOnsubscribe=function (){

    this.LoadserverClient.subscribe('testMessage');
    this.LoadserverClient.subscribe('DeviceEvent');  //订阅ZigbeeServer  Device设备上报状态信息
    this.LoadserverClient.subscribe('Return_cmd');    //订阅ZigbeeServer  处理结果返回
    this.LoadserverClient.subscribe('returnGatewayMac');    //订阅ZigbeeServer  返回GateWayMac地址
    this.LoadserverClient.subscribe('AddDevice');//订阅ZigbeeServer 如果有节点加入向这个节点加入
    this.emit('LoadServerConnectSuccess');
}



module.exports = startLoadServerMqtt;